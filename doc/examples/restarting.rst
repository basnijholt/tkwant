:orphan:

.. _restarting:

Restarting calculations from previously saved results
=====================================================

The physical system in this example is similar to :ref:`open_system`.
For time arguments on the right of the dashed vertical line, the
result is calculated with the restarted wave function.

**tkwant features highlighted**

-  restarting calculations from previously saved results

The code can be also found in 
:download:`restarting.py <restarting.py>`.

.. jupyter-execute:: restarting.py

