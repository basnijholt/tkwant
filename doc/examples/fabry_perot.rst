:orphan:

.. _fabry_perot:

Fabry-Perot interferometer
==========================

**Physics Background**

Applying voltage pulses to Fabry-Perot cavities to illustrate dynamic
control of interference. The example is taken from the Tkwant article `[1] <#references>`__,
but with a shorter simulation time and a reduced accuracy
in order to speed up the calculation.

The code can be also found in
:download:`fabry_perot.py <fabry_perot.py>`.

.. jupyter-execute:: fabry_perot.py

References
----------

[1] B. Gaury, J. Weston, X. Waintal,
`The a.c. Josephson effect without superconductivity 
<https://www.nature.com/articles/ncomms7524>`__, 
Nat. Commun. **6**, 6524 (2015).
`[arXiv] <https://arxiv.org/abs/1407.3911>`__

