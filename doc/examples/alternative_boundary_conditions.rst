:orphan:

.. _alternative_boundary_conditions:

Alternative boundary conditions
===============================

The physical system in this example is similar to :ref:`open_system`.

**tkwant features highlighted**

- Selecting boundary conditions manually

- Impact of the boundary type on performance

The code can be also found in
:download:`alternative_boundary_conditions.py <alternative_boundary_conditions.py>`.

.. jupyter-execute:: alternative_boundary_conditions.py

