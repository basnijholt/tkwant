:mod:`tkwant.mpi` -- Parallelizing simulations with the Message Passing Interface (MPI)
=======================================================================================

.. module:: tkwant.mpi

MPI helper functions
--------------------

.. autosummary::
    :toctree: generated

    communicator_init
    communicator_free
    get_communicator
    distribute_dict
    DistributedDict
    round_robin



