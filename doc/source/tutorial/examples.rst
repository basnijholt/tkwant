.. _examples:

Examples
========

**Onebody dynamics**

:ref:`closed_system`

:ref:`open_system`

:ref:`restarting`

:ref:`alternative_boundary_conditions`

**Manybody dynamics**

:ref:`voltage_raise`

:ref:`fabry_perot`
