.. _introduction:

Introduction
============

Tkwant is a Python package for the simulation of quantum nanoelectronics devices on which
external time-dependent perturbations are applied. Tkwant is an extension of the `Kwant <https://kwant-project.org>`_  package
and can handle the same types of systems: discrete tight-binding like
models that consist of an arbitrary central region connected to semi-infinite electrodes, also called leads.
For such systems, tkwant provides algorithms to simulate the time-evolution of manybody expectation values, as e.g. densities and currents.

.. _system:

.. figure:: scattering_region.png
    :width: 500px


    Sketch of a typical open quantum system which can
    be simulated with Tkwant. A central scattering region (in
    black) is connected to several leads (in grey). Each lead
    represents a translationally invariant, semi-infinite system in
    thermal equilibrium. Sites and hopping matrix elements are
    represented by dots and lines. The regions in red indicate a
    time-dependent perturbation, in this example a global voltage
    pulse :math:`V_p (t)` on lead 0 and a time-dependent voltage :math:`V_g (t)` on
    a gate inside the scattering region.
    The figure is taken out of `Tkwant's main paper <https://kwant-project.org/extensions/tkwant/pre/about#citation>`__.


We refer to `Tkwant's main paper <https://kwant-project.org/extensions/tkwant/pre/about#citation>`__ for the theoretical background.


General workflow for using Tkwant
---------------------------------
Tkwant is a Python package and a small Python script must be written to perform
a simulation. This approach is similar to `Kwant <https://kwant-project.org>`_ and
most of the script, basically the first two points listed below, require only Kwant: 

**Building the system**

    The scattering region with the leads is build using Kwant.
    Time-dependent potentials and couplings can be defined straightforewardly
    and the *time* variable appears just as a regular function parameter.

**Defining operators**

    Physical observables can be measured by defining the corresponding operators.
    Predefined operators, as for the current and the density, can be
    used directly from `Kwant's operator module <https://kwant-project.org/doc/1/reference/kwant.operator>`_.   

**Solving the time-dependent equations**

    The time-dependent manybody state can be constructed mostly automatically
    from the Kwant system. Looping over discrete time steps, the state
    is evolved forwardly and operators can be applied along the way to
    provide expectation values. The simulated observables can be plotted directly or
    saved for later postprocessing by means of standard Python tools. 


Outline of the tutorial
-----------------------

The first tutorial section 
:ref:`getting_started`
dives directly into Tkwant with a simple toy example.
The next sections
:ref:`onebody`, :ref:`manybody` and :ref:`time_dep_system` explain the core concepts of Tkwant.
They are important to understand in order to use Tkwant effectively and to be able
to perform realistic simulations.
Technical details are covered in the sections
:ref:`onebody_advanced`,
:ref:`manybody_advanced`
and
:ref:`boundary`. 
These sections can be skipped in a first
tour and are intended for advanced users who are interested to change the default behavior.
The two sections
:ref:`mpi` and
:ref:`logging`
introduce both topics at a beginner's level and give some hands-on advice. 
Although not necessary for a basic understanding, these topics are likely to be
relevant for performing realistic simulations. 
the
:ref:`examples`
section summarizes several complete Tkwant examples.
Learning directly from the examples might be a good starting point especially for experienced Kwant users.
Finally, the 
:ref:`faq`
section covers possible pitfalls.


