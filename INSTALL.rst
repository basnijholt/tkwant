.. _install:

Installation
============

This section covers the installation of tkwant on a GNU/Linux
system as Debian and Ubuntu via the command line.

Requirements
^^^^^^^^^^^^

Tkwant requires several non-Python dependencies:

- C compiler (eg. `gcc <https://gcc.gnu.org/>`_)
- `BLAS <http://www.netlib.org/blas/>`_ (eg. `OpenBLAS <http://www.openblas.net/>`_)
- `Sparse BLAS <http://librsb.sourceforge.net/>`_
- `MPI <https://www.mpi-forum.org/>`_ (eg. `Open MPI <https://www.open-mpi.org/>`_)
- `Kwant <https://kwant-project.org/>`_

The non-Python dependencies of Tkwant can be installed with the following command::

   sudo apt-add-repository -s ppa:kwant-project/ppa
   sudo apt-get update
   sudo apt-get install build-essential libopenblas-dev librsb-dev libopenmpi-dev python3-kwant

Tkwant requires at least Python 3.4. The following Python packages must
be installed to build tkwant:

- `Cython <https://cython.org/>`_
- `NumPy <https://numpy.org/>`_
- `SciPy <https://www.scipy.org/>`_
- `SymPy <https://www.sympy.org/en/index.html>`_
- `mpi4py <https://mpi4py.readthedocs.io/en/stable/>`_
- `dill <https://dill.readthedocs.io/en/latest/dill.html>`_
- `tinyarray <https://pypi.org/project/tinyarray/>`_
- `kwantSpectrum <https://kwant-project.org/extensions/kwantspectrum/>`_

The following software is recommended, even though not needed to build tkwant:

- `matplotlib <https://matplotlib.org/>`_

All Python packages can be installed from the command line
by the standard Python package manager `pip <https://pip.pypa.io/en/stable/>`_ via::

    python3 -m pip install --user cython numpy scipy sympy mpi4py dill tinyarray kwantspectrum matplotlib

Above *pip* command can be also used within the Anaconda Python distribution,
see the instructions for `Conda <#conda>`__.
For version requirements we refer to the *requirements* section in file
``setup.py`` in the project repository.

Tkwant needs additional packages for running tests or to build the documentation.
These additional packages are not mandatory for building tkwant however.

Testing requirements
--------------------
The tkwant test suite requires the following Python packages:

- `pytest <https://docs.pytest.org/en/latest/>`_
- `pytest-cov <https://pytest-cov.readthedocs.io/en/latest/>`_
- `pytest-flake8 <https://pypi.org/project/pytest-flake8/>`_

The packages can be installed by the standard *pip* command::

    python3 -m pip install --user pytest pytest-cov pytest-flake8


Documentation requirements
--------------------------
Building the documentation requires the following Python packages:

- `sphinx <https://www.sphinx-doc.org/en/master/>`_
- `jupyter-sphinx <https://jupyter-sphinx.readthedocs.io/en/latest/>`_
- `matplotlib <https://matplotlib.org/>`_

The packages can be installed by the standard *pip* command::

    python3 -m pip install --user sphinx jupyter-sphinx matplotlib


Installating tkwant from source
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tkwant is a Python package that includes Cython modules.
At this point, tkwant must be installed from source (`official tkwant git repository <https://gitlab.kwant-project.org/kwant/tkwant>`_).
The `installation instructions of Kwant <https://kwant-project.org/doc/1/pre/install>`_ apply mostly also to tkwant, but in many cases installation will be as simple as::

    python3 -m pip install --user git+https://gitlab.kwant-project.org/kwant/tkwant.git

Installing tkwant is convenient for users which only like to use the existing tkwant module.
If one is interested to also modify or develop the tkwant code,
the instructions "Building tkwant for development" described below are more appropriate.

Building tkwant for development
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For development, tkwant should first be cloned from the tkwant repository::

    git clone https://gitlab.kwant-project.org/kwant/tkwant.git

Then, after *cd* into the local repository,
one can locally build tkwant with the command::

    python3 setup.py build_ext -i

In order to have the tkwant module in the Python search path,
one can make a symbolic link to the tkwant folder. The following command can be
adapted to create the symlink::

    mkdir -p ~/.local/lib/python3.X/site-packages/
    ln -s ABSOLUTE-PATH-TO-TKWANT-REPO/tkwant ~/.local/lib/python3.X/site-packages/

where ``python3.X`` must be replaced by the correct folder name
in the Python search path. Note that ``../tkwant`` refers to the directory tkwant
located inside the local tkwant repository.

Conda
^^^^^
.. _conda:

Tkwant provides no dedicated Anaconda support. It is however possible to
install packages via *pip* and *git* similarly with conda.
First, *pip* and *git* must be installed for conda with::

    conda install git pip

All required Python packages and also Tkwant can then be installed with above commands.

macOS and Windows
^^^^^^^^^^^^^^^^^
Tkwant provides no installers for both platforms. It should be possible however
to install Tkwant via `Conda <#conda>`__ as described above.
